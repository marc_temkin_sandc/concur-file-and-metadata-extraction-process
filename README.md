# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

1. Read incoming folder for one or more zips.
	a. Each zip represents an invoice
	b. Keep track of split zips using the _{x}of{y} suffix in the filename

2. Create folders for each invoice
	a. Extract the manifest file
	b. Extract the invoice.
	b. Extract the embedded zip file(s) as sub-folder(s)


3. For each file in the expenses sub-folders
	a.Determine the metadata from the manifest.
	b.Associate the metadata to each file.
		1. Also keep the metadata as a separate file to use in loading to Sharepoint

4. Transfer all files with metadata to Sharepoint
	a. Create folders in correct country silo in Expenses
	b. Create folder in Invoice
	c. Transfer the files to the correct folders along with metadata.

5. Notify of completion		 
	

	


