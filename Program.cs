﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace ConcurFileMetadataExtraction
{
    class Program
    {

        /// <summary>
        /// The paths to the source and destination folders stored in the 
        /// appsettings.json file.
        /// </summary>
        private static string downloadFolder, extractFolder;
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            using IHost host = CreateHostBuilder(args).Build();
            System.Console.WriteLine($"{downloadFolder}:{extractFolder}");
            if (!new ExtractProcess(downloadFolder, extractFolder).Process(out string errorMessage))
            {
                System.Console.WriteLine($"Extract Process error:{errorMessage}");
            }
        }

        static IHostBuilder CreateHostBuilder(string[] args) => Host.CreateDefaultBuilder(args).
            ConfigureAppConfiguration((hostingContext, configuration) =>
            {
                configuration.Sources.Clear();
                IHostEnvironment env = hostingContext.HostingEnvironment;

                configuration.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

                IConfigurationRoot configurationRoot = configuration.Build();
                downloadFolder = configurationRoot.GetValue<string>("ConcurDownloadFolder");
                extractFolder = configurationRoot.GetValue<string>("ExtractFolder");
            });
    }


}
