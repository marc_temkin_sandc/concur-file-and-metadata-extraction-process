using System.Collections.Generic;

namespace ConcurFileMetadataExtraction
{
    internal class FilesByInvoice
    {

        /// <summary>
        /// The basename of the file which will be used as the folder name
        /// </summary>
        internal string FileBaseName;

        /// <summary>
        /// List of one or more files based on the filename pattern
        /// </summary>
        internal List<string> Files;

        /// <summary>
        /// Constructor used when there is a single file as in 
        /// extract_images_phos(number pattern)_(number pattern)1of1.zip
        /// </summary>
        /// <param name="baseName">The filename without the extension</param>
        /// <param name="singleFile">The entire file that ends in a 1of1 suffix</param>
        public FilesByInvoice(string baseName, string singleFile)
        {
            this.FileBaseName = baseName;
            Files = new List<string> { singleFile };
        }

        public FilesByInvoice(string baseName, IEnumerable<string> fileNames)
        {
            this.FileBaseName = baseName;
            Files = new List<string>(fileNames);
        }

        /// <summary>
        /// Return the number of zip files for this invoice
        /// </summary>
        internal int FileCount => Files.Count;

        /// <summary>
        /// Returns true if there is only one zip file
        /// </summary>
        internal bool HasSingleZip => FileCount == 1;
    }
}