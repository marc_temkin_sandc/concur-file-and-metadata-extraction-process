using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;

namespace ConcurFileMetadataExtraction
{

    /// <summary>
    /// Begins the file/folder extraction process for the Concur import files.
    /// </summary>
    internal class ExtractProcess
    {
        /// <summary>
        /// The base directory where the downloaded files are stored
        /// </summary>
        private string _downloadFolder;

        /// <summary>
        /// The base directory where the extracted files/folders are stored
        /// </summary>
        private string _extractFolder;

        /// <summary>
        /// Pattern to find zip files that have a one-to-one relationship with an invoice.
        /// This is determined by a zip file ending in _1of1.zip
        /// </summary>
        private Regex _multiFileExpr = new("extract_images_phos\\d+_\\d+_(\\d)of\\1.zip");

        /// <summary>
        /// Pattern to find zip that files that may have a one-to-many relationship with an invoice.
        /// This is determined by a zip file ending in i  of n zip where n is greater than 1 and 
        /// i is less than n
        /// </summary>
        Regex _multiFileExtractBaseName = new("(extract_images_phos\\d+_\\d+)_\\d+of\\d+.zip");

        /// <summary>
        /// Initialize the process
        /// </summary>
        /// <param name="downloadFolder">The home folder for the Concur downloads</param>
        /// <param name="extractFolder">The home folder where the files/folders are extracted</param>
        internal ExtractProcess(string downloadFolder, string extractFolder)
        {
            this._downloadFolder = downloadFolder;
            this._extractFolder = extractFolder;
        }

        /// <summary>
        /// The main process and only externally accessible method
        /// </summary>
        internal bool Process(out string errorMessage)
        {
            errorMessage = String.Empty;

            // Standard error checking on existence of directory
            if (String.IsNullOrWhiteSpace(_downloadFolder) || String.IsNullOrWhiteSpace(_extractFolder) )
            {
                errorMessage = "No specified download folder or extract folder given!";
                return false;
            }
            DirectoryInfo di = new(this._downloadFolder);
            if (!di.Exists)
            {
                errorMessage = $"Specified download folder, {_downloadFolder}, does not exist!";
                return false;
            }

            int numFolders = CreateWorkingFolders(di);
            System.Console.WriteLine(numFolders);


            return true;
        }

        /// <summary>
        /// Extract the zip files and create the working directories
        /// </summary>
        /// <param name="di">The Directory Info instance</param>
        /// <returns>The number of folders created</returns>
        private int CreateWorkingFolders(DirectoryInfo di)
        {
            FileInfo[] allFiles = di.GetFiles("*.zip");

            // Screen for invoices with multiple zips
            // Will match a pattern (filename)iofn where 
            // i is greater than 1

            List<FilesByInvoice> filesByInvoice = SeparateByPattern(allFiles);

            // Create folders for each filesByInvoice instance
            // with their respective files, sub-folders and their files. (Recursive)

            // Create the extract folder if it does not already exist
            if (filesByInvoice.Sum(f => f.FileCount) > 0)
            {
                if (!Directory.Exists(_extractFolder))
                    Directory.CreateDirectory(_extractFolder);
            }

            // Extract each zip file 
            foreach (FilesByInvoice finv in filesByInvoice)
            {
                
                if (finv.HasSingleZip)
                {
                    string extractToFolderPath = Path.Combine(_extractFolder, finv.FileBaseName);
                    string extractZipFromPath = Path.Combine(_downloadFolder, finv.Files.First());
                    ZipArchive archive = ZipFile.OpenRead(extractZipFromPath);
                    archive.ExtractToDirectory(extractToFolderPath);

                    List<ZipArchiveEntry> subZips =        
                    archive.Entries.Where(ae => ae.FullName.EndsWith("zip")).ToList();
                    ExpandSubFolderZipFiles(extractToFolderPath, subZips);

                }
                else
                {
                    foreach (var zipfilename in finv.Files)
                    {
                        string extractToFolderPath = Path.Combine(_extractFolder, finv.FileBaseName);
                        string extractZipFromPath = Path.Combine(_downloadFolder, zipfilename);
                        ZipArchive archive = ZipFile.OpenRead(extractZipFromPath);
                        archive.ExtractToDirectory(extractToFolderPath);

                        List<ZipArchiveEntry> subZips =        
                        archive.Entries.Where(ae => ae.FullName.EndsWith("zip")).ToList();
                        ExpandSubFolderZipFiles(extractToFolderPath, subZips);
                    }

                }
                
            }

            return filesByInvoice.Count;
        }

        private static void ExpandSubFolderZipFiles(string extractToFolderPath, List<ZipArchiveEntry> subZips)
        {
            
            foreach (ZipArchiveEntry ze in subZips)
            {
                string archiveFolderName = Path.Combine(extractToFolderPath, ze.FullName[0..^4]);
                string tempFile = Path.GetTempFileName();
                tempFile = tempFile.Replace("tmp", "zip");
                ze.ExtractToFile(tempFile);
                using (ZipArchive archive = ZipFile.OpenRead(tempFile))
                    archive.ExtractToDirectory(archiveFolderName);
    
                File.Delete(tempFile);
            }

        }

        /// <summary>
        /// Organize the zip files into separate FilesByInvoice instances which 
        /// will store a foldername that corresponds to the invoice and the
        /// zip(s) attached to the Invoice.
        /// </summary>
        /// <param name="allFiles">All of the zip files in the home folder</param>
        /// <returns>A list of Files By Invoice</returns>
        private List<FilesByInvoice> SeparateByPattern(FileInfo[] allFiles)
        {

            bool allSingleZips = allFiles.All(f => _multiFileExpr.IsMatch(f.Name));
            System.Console.WriteLine($"All Files Match:{allSingleZips}");

            // Extract the left-most part of the name up to the i of N pattern.zip
            List<FilesByInvoice> filesByInvoices = new();

            // All invoices have only one zip per invoice
            if (allSingleZips)
            {
                filesByInvoices = CollectInvoiceWithSingleZips(allFiles);
            }
            // Invoices may have one or more zips
            else
            {
                // Get the base name used by the zip files
                HashSet<string> baseNames =
                allFiles.Select(f => _multiFileExtractBaseName.IsMatch(f.Name) ?
                 _multiFileExtractBaseName.Match(f.Name).Groups[1].Value : String.Empty).ToHashSet();

                // Process each base name to determine if the basename has one or more zip files.
                foreach (string baseName in baseNames)
                {
                    // These are the invoices that only have a single file
                    if (allFiles.Where(fi => fi.Name.StartsWith(baseName)).All(f => _multiFileExpr.IsMatch(f.Name)))
                    {
                        filesByInvoices.Add(
                            new FilesByInvoice(baseName, allFiles.First(fi => fi.Name.StartsWith(baseName)).FullName));
                    }
                    else
                    {
                        // These invoices have multiple zip files
                        filesByInvoices.Add(
                        new FilesByInvoice(baseName,
                        allFiles.Where(fi => fi.Name.StartsWith(baseName)).Select(fi => fi.FullName)));

                    }
                }

            }

            return filesByInvoices;

        }

        /// <summary>
        /// Create a folder for each invoice as each has a single zip
        /// </summary>
        /// <param name="allFiles"></param>
        /// <returns></returns>
        private List<FilesByInvoice> CollectInvoiceWithSingleZips(FileInfo[] allFiles)
        {
            List<FilesByInvoice> temp = new();
            foreach (FileInfo fi in allFiles)
            {
                Match mc = _multiFileExtractBaseName.Match(fi.Name);
                if (mc.Success)
                {
                    string foldername = mc.Groups[1].Value;
                    temp.Add(new FilesByInvoice(foldername, fi.FullName));
                }
            }
            return temp;
        }






    }
}